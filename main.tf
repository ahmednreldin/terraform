provider "aws" {}

# Define Variables
variable cidr_blocks {
  type = list( object({
               cidr_block = string,
               name = string
              }))
  description = "cider blocks values"
}

variable environment {
  type = string
  description = "Deployment environment"
}


# # -------- Define Resources i.e. create new resources -------- # 
# # Create VPC
# resource "aws_vpc" "dev-vpc" {
#   cidr_block = var.cidr_blocks[0].cidr_block

#   tags = {
#     Name = "${var.cidr_blocks[0].name} - ${var.environment}"  # string interpolation for concatination, + not allowed 
#   }
# }
# # Create Subnet
# resource "aws_subnet" "dev-subnet" {
#   vpc_id     = aws_vpc.dev-vpc.id
#   cidr_block = var.cidr_blocks[1].cidr_block

#   tags = {
#      Name = "${var.cidr_blocks[1].name} - ${var.environment}" 
#   }
# }
# # -------- Define Data source i.e. Fetch existing resource from provider -------- # 

# data "aws_vpc" "existing-vpc"{
#     default = true
# }

# resource "aws_subnet" "dev-subnet-2"{
#     vpc_id = data.aws_vpc.existing-vpc.id
#     cidr_block = "172.31.48.0/20" # cider block of default VPC

#     tags = {
#       Name = "Subnet Created by Terraform"
#     }
# }

# # ------ Output Attribute values after apply ----# 
# output "development-vpc"{
#   value = aws_vpc.dev-vpc.id
# }

# output "development-subnet"{
#   value = aws_subnet.dev-subnet.id
# }


